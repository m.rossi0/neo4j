from neo4j import GraphDatabase
import pyinputplus as pyip
import datetime

"""
Applicazione per la Polizia Postale

Obiettivo:
La Polizia Postale vuole sapere se una persona era presente in un certo luogo in un intervallo di date. 
Per fare ciò controlla se uno dei telefoni di proprietà di quella persona si è collegato ad una cella.

"""


class PoliziaPostale:

    def __init__(self, neo4j_uri, neo4j_username, neo4j_password, neo4j_db):
        self.driver = GraphDatabase.driver(neo4j_uri, auth=(neo4j_username, neo4j_password))
        self.session = self.driver.session(database=neo4j_db)

    def scollegamento(self):
        self.session.close()
        self.driver.close()

    def filtro_nominativo(self):
        """
        Localizzare una persona sospetta:
        Con una data, un orario e una persona, elencare le celle telefoniche
        alle quali le SIM intestate a quella persona erano collegate.
        """

        lista_nominativi = []
        nominativo = self.session.run("match(p:Persona) return p.nome").value()
        cognomi = self.session.run("match(p:Persona) return p.cognome").value()
        for i, b in zip(nominativo, cognomi):
            lista_nominativi.append(i + " " + b)
        print('Ecco la lista delle persone presenti nel nostro database:')
        for i, persona in enumerate(lista_nominativi):
            print(f"{i + 1}: {persona}")
        number = pyip.inputNum(prompt="Inserisca il numero su cui vuole filtrare\n"
                                      "->",
                               min=1, max=len(lista_nominativi))

        index = lista_nominativi[number - 1].index(" ")
        nome = lista_nominativi[number - 1][:index]
        cognome = lista_nominativi[number - 1][index + 1:]

        while True:
            try:
                data_inizio = input('Inserisci la data di inizio nel formato YYYY-MM-DD HH:MM\n'
                                    '->')
                date_time_obj_inizio = datetime.datetime.strptime(data_inizio, '%Y-%m-%d %H:%M')
                time_stamp_inizio = date_time_obj_inizio.timestamp()
                break
            except ValueError:
                print('Riprova e segui le istruzioni.')

        while True:
            try:
                data_fine = input('Inserisci la data di fine nel formato YYYY-MM-DD HH:MM\n'
                                  '->')
                date_time_obj_fine = datetime.datetime.strptime(data_fine, '%Y-%m-%d %H:%M')
                time_stamp_fine = date_time_obj_fine.timestamp()
                break

            except ValueError:
                print('Riprova e segui le istruzioni.')

        inizio_fine = self.session.run("MATCH (p:Persona{nome:$nome,cognome:$cognome})-[:POSSIEDE]->(s:Sim)"
                                       "MATCH (s:Sim{numero:s.numero})-[d:DATA]->(t:Antenna) return d.inizio, d.fine",
                                       nome=nome, cognome=cognome).values()
        print('Nelle date indicate le SIM della persona indicata si sono trovate qui:')
        for i in range(len(inizio_fine)):
            if float(time_stamp_fine) > float(time_stamp_inizio) and (
                    time_stamp_inizio <= float(inizio_fine[i][0]) and time_stamp_fine >= float(inizio_fine[i][1])):
                cella = self.session.run(
                    "MATCH (p:Persona{nome:$nome,cognome:$cognome})-[:POSSIEDE]->(s:Sim)"
                    "MATCH (s:Sim{numero:s.numero})-[d:DATA]->(t:Antenna) return t.nome", nome=nome,
                    cognome=cognome).value()[i]
                dt_object_inizio = datetime.datetime.fromtimestamp(float(inizio_fine[i][0]))
                dt_object_fine = datetime.datetime.fromtimestamp(float(inizio_fine[i][1]))
                print(f"- Dal {dt_object_inizio} al {dt_object_fine} nella cella {cella}")


    def filtro_torretta(self):
        """
        Trovare Sospetti in una zona di reato:
         Con una data, un orario ed una cella, elencare le persone intestatarie delle SIM
         collegate a quella cella in quel momento
        """
        while True:
            try:
                lista_torretta = []
                torretta = self.session.run("match(n:Antenna) return n.nome").value()
                print('Ecco la lista delle antenna:')
                for (i, cella) in enumerate(torretta):
                    lista_torretta.append(cella)
                    print(f"{i + 1}: {cella}")
                number = int(input("Inserisci il numero dell'antenna che vuoi filtrare: \n"
                                   "->"))
                if number in range(len(lista_torretta) + 1):
                    scelta = lista_torretta[number - 1]
                    date_time_str = input('Inserisci la data nel formato YYYY-MM-DD HH:MM\n'
                                          '->')
                    date_time_obj = datetime.datetime.strptime(date_time_str, '%Y-%m-%d %H:%M')
                    time_stamp = date_time_obj.timestamp()
                    inizio = self.session.run('MATCH (s:Sim)-[d:DATA]->(t:Antenna{nome:$scelta}) return d.inizio',
                                              scelta=scelta).value()
                    fine = self.session.run('MATCH (s:Sim)-[d:DATA]->(t:Antenna{nome:$scelta}) return d.fine',
                                            scelta=scelta).value()
                    print(f"Le SIM collegate all'antenna sono:")
                    for i in range(len(inizio)):
                        if time_stamp >= float(inizio[i]) and time_stamp <= float(fine[i]):
                            numero = self.session.run(
                                'MATCH (s:Sim)-[d:DATA]->(t:Antenna{nome:$scelta})MATCH (n:Persona)-[:POSSIEDE]->(s:Sim{numero:s.numero}) return n.nome, n.cognome, s.numero',
                                scelta=scelta).values()[i]
                            print(f"-> {numero[0]} {numero[1]} (con il numero di telefono +39{numero[2]})")
                    break
                else:
                    print('Hai inserito un valore errato, ritenta.')

            except ValueError:
                print('Riprova e segui le istruzioni.')

    def SospettiPerZona(self, posizione, date_time):
        """
        Trovare Sospetti in una zona di reato:
        Date delle coordinate geografiche, una data e un orario elencare le persone intestatarie delle SIM
        collegate alle celle che si trovano in un certo raggio dalle coordinate date
        """
        distances = {}
        timestamp = datetime.datetime.strptime(date_time, '%Y-%m-%d %H:%M').timestamp()
        location = self.session.run(query="""CALL apoc.spatial.geocodeOnce($posizione)
                                            YIELD location
                                            RETURN location.latitude, location.longitude""",
                                    posizione=posizione).data()[0]

        query = """match (a:Antenna)
                WITH
                  point({longitude: $longitude, latitude: $latitude}) AS p1,
                  point({latitude:a.latitudine, longitude:a.longitudine}) AS p2

                RETURN point.distance(p1, p2)/1000 as dist """
        dist = self.session.run(query=query, longitude=location["location.longitude"],
                                latitude=location["location.latitude"]).value()

        names = self.session.run("MATCH (a:Antenna) return a.id").value()
        for distance, name in zip(dist, names):
            distances[name] = distance

        distances = dict(sorted(distances.items(), key=lambda item: item[1]))
        d = list(distances.keys())
        persone = self.session.run(query="""match (a:Antenna{id:$id_antenna})-[r:DATA]-(sim)-[:POSSIEDE]-(persona) 
                                            where toInteger(r.inizio) <= $date_time and toInteger(r.fine) >= $date_time
                                            return persona.nome, persona.cognome, sim.numero""",
                                   id_antenna=d[0], date_time=timestamp).data()
        return persone


if __name__ == "__main__":
    obj = PoliziaPostale("bolt://localhost:7687", "neo4j", "password", "")
    while True:
        number = pyip.inputNum(prompt="Buongiorno e benvenuto nell'applicazione personale della Polizia Postale.\n"
                                      "Scelga il numero del filtro:\n"
                                      "1) Localizzare una persona sospetta\n"
                                      "2) Trovare sospetti in una zona di reato in base al nome dell'antenna\n"
                                      "3) Trovare sospetti in una zona di reato date le coordinate geografiche \n"
                                      "0) Esci\n"
                                      "->",
                               min=0, max=4)

        if number == 1:
            obj.filtro_nominativo()

        elif number == 2:
            obj.filtro_torretta()


        elif number == 3:
            posizione = input("Inserisci la via e la città: \n->")
            while True:
                try:
                    date_time = input('Inserisci la data nel formato YYYY-MM-DD HH:MM \n-> ')

                    res = obj.SospettiPerZona(posizione, date_time)

                    print("Persone collegate:")
                    for item in res:
                        print(
                            f"Nome: {item['persona.nome']}, Cognome: {item['persona.cognome']}, numero di cellulare: {item['sim.numero']}")
                    break
                except ValueError:
                    print('Hai inserito un valore errato, ritenta.')


        elif number == 0:
            obj.scollegamento()
            break
